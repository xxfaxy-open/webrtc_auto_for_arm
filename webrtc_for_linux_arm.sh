#!/bin/bash
unset GREP_OPTIONS
sudo apt-get install -y python-dev
#
mkdir /home/ubuntu/webrtc_data
mkdir /home/ubuntu/webrtc_data/ssl
mkdir /home/ubuntu/webrtc_data/boost
mkdir /home/ubuntu/webrtc_data/arm_tool
mkdir /home/ubuntu/webrtc_data/websocket
mkdir /home/ubuntu/webrtc_data/webrtc_checkout
mkdir /home/ubuntu/webrtc_data/code
#
cd /home/ubuntu/webrtc_data/ssl
git clone https://boringssl.googlesource.com/boringssl
#
cd /home/ubuntu/webrtc_data/arm_tool
wget -c https://releases.linaro.org/components/toolchain/binaries/6.3-2017.05/arm-linux-gnueabihf/gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf.tar.xz
tar -xvf gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf.tar.xz
rm ./gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf.tar.xz
export PATH=$PATH:/home/ubuntu/webrtc_data/arm_tool/gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf/bin
source /etc/profile
#
cd /home/ubuntu/webrtc_data/boost
wget -c https://dl.bintray.com/boostorg/release/1.65.1/source/boost_1_65_1.tar.gz
tar -xzvf ./boost_1_65_1.tar.gz
rm ./boost_1_65_1.tar.gz
cd /home/ubuntu/webrtc_data/boost/boost_1_65_1
./bootstrap.sh
cp /home/ubuntu/project-config.jam /home/ubuntu/webrtc_data/boost/boost_1_65_1/project-config.jam
./b2 --build-type=minimal --toolset=gcc-arm variant=release link=static threading=multi runtime-link=shared --with-iostreams --with-system
#
cd /home/ubuntu/webrtc_data/websocket
git clone https://github.com/zaphoyd/websocketpp.git
cp /home/ubuntu/tls.hpp /home/ubuntu/webrtc_data/websocket/websocketpp/websocketpp/transport/asio/security/tls.hpp
#
cd /home/ubuntu/webrtc_data
git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
export PATH=$PATH:/home/ubuntu/webrtc_data/depot_tools
export GYP_DEFINES="target_arch=arm"
source /etc/profile
#
cd /home/ubuntu/webrtc_data/webrtc_checkout
fetch --nohooks webrtc
gclient sync
#
cd /home/ubuntu/webrtc_data/webrtc_checkout/src
./build/install-build-deps.sh
gn gen out/arm/Release --args='is_debug=false rtc_use_h264=true ffmpeg_branding="Chrome" rtc_include_tests=false is_clang=false target_cpu="arm" treat_warnings_as_errors=false rtc_enable_protobuf=false symbol_level=0 use_rtti=true use_custom_libcxx=false use_custom_libcxx_for_host=false rtc_use_gtk=false  use_gtk3=false'
ninja -C out/arm/Release
#
