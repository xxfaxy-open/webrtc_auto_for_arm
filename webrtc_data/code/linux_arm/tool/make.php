<?php
$config = array(
    'bin_name' => 'webrtc_main',
    'webrtc_data_path' => '/home/php/webrtc/arm/webrtc_data',
    'src_path' => '@webrtc_data_path/webrtc_checkout/src'
);
$config['src_path'] = str_replace('@webrtc_data_path', $config['webrtc_data_path'], $config['src_path']);

function getFileList($file){
    global $config;
    $list = file($file);
    foreach($list as $k => $v){
        $temp = trim($v);
        if(empty($temp)){
            unset($list[$k]);
        }
        else{
            $temp = str_replace('@webrtc_data_path', $config['webrtc_data_path'], $temp);
            $temp = str_replace('@src_path', $config['src_path'], $temp);
            $list[$k] = $temp;
        }
    }
    return $list;
}

$D = implode(' ', getFileList('list/D.txt'));
$lib = implode(' ', getFileList('list/lib.txt'));
$code = implode(' ', getFileList('list/code.txt'));
$Include = implode(' ', getFileList('list/Include.txt'));
$Location = implode(' ', getFileList('list/Location.txt'));
$libStatic = implode(' ', getFileList('list/libStatic.txt'));

$command = "arm-linux-gnueabihf-g++ -O2 -Wall -std=c++11 -o {$config['bin_name']} {$code} {$D} {$Include} {$Location} {$libStatic} {$lib}";

$content = array(
    '#!/bin/bash',
    'export PATH=$PATH:' . $config['webrtc_data_path'] . '/arm_tool/gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf/bin',
    $command
);
$content = implode(PHP_EOL, $content);
file_put_contents('../c.sh', $content);
?>
