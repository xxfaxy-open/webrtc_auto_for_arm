把下面的3个文件放到`ubuntu 16.04 server`主机的`/home/ubuntu`目录下

tls.hpp

project-config.jam

webrtc_for_linux_arm.sh

然后运行

```
cd /home/ubuntu
./webrtc_for_linux_arm.sh
```

自己项目的代码应该放到`/home/ubuntu/webrtc_data/code`里面，最好每个项目都有独立的子目录
